# Lab - Excercise 1
This excercise will provide hands-on on how to use Open SSL with NGINX Ingress Controller.

## Lab Objective
In this lab we will look at generating Open SSL Certificate and using in NGINX Ingress Controller.

## Prequisite
This are following setup which is required before starting this lab.
* Kubernetes Cluster fully running in Azure. You can also make use of terraform scripts from lab 1.
* Kubernetes environment must be accessible from your machine.
* Git should be working from your machine.
* OpenSSL is also required.
* HRLM Chart should also be installed.

For Creating the Azure Kubernetes Service, You can use  below Azure CLI command -
```bash
RG=sas-tls-lab
az aks create \ --resource-group $RG \ --name Cluster01 \ --node-count 3 \ --generate-ssh-keys \ --node-vm-size Standard_B2s \ --enable-managed-identity

az aks get-credentials --name Cluster01 --resource-group $RG
```

## Install Ingress Controller
First step is to create the namespace and deploy NGINX Ingress Controller in kubernetes cluster. NGINX Ingress Controller acts like Front door and in AKS it make use of Load alancer provided by Azure.

```bash
# Create a namespace for your ingress resources
kubectl create namespace ingress-basic

# Add the ingress-nginx repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

# Use Helm to deploy an NGINX ingress controller
helm install nginx-ingress ingress-nginx/ingress-nginx \
    --namespace ingress-basic \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set controller.admissionWebhooks.patch.nodeSelector."beta\.kubernetes\.io/os"=linux

kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller
```

## Create Self Signed Certificate
Create the Self Signed Certificate for common name demo.saslab.com. Self Signed Certificate can be used for Test, UAT but shouldn't be used for Production use. Once the below command is executed the output will be ingress-tls.crt and ingress-tls.key which will be further used.

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -out ingress-tls.crt \
    -keyout ingress-tls.key \
    -subj "/CN=demo.saslab.com/O=ingress-tls"
```


## Validate Created Certificate
Certificate what was created in previous can be validated using below command.

```bash
openssl x509 -noout -text -in ./ingress-tls.crt
```

## Create Secrets of the related Certificate
Create secret to store certificate and key.

```bash
kubectl create secret tls ingress-tls \
    --namespace ingress-basic \
    --key ingress-tls.key \
    --cert ingress-tls.crt
```

## Deploy Demo Application into Created Clusters
To test how certificate working, we will deploy one sample application in kubernetes cluster. YAML manifest for sample application is already present in Git. Run below script to deploy.

```bash
kubectl apply -f https://gitlab.com/bhaveshraval/tls-lab/-/blob/main/app-deploy.yaml
```

## Map TLS certificate in Ingress Controller 

Create the ingress-controller.yaml and copy and paste above script. 
```bash
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: saslab-ingress
  namespace: ingress-basic
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  tls:
  - hosts:
    - demo.saslab.com
    secretName: ingress-tls
  rules:
  - host: demo.saslab.com
    http:
      paths:
      - path: /(.*)
        pathType: Prefix
        backend:
          service:
            name: saslab
            port:
              number: 80
```
Apply the created file

```bash
kubectl apply -f ingress-controller.yaml
```

## Check if the TLS is ready to be tested
Runnbelow command to check the EXTERNAL_IP which can eb used for domain mapping.

```bash
kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller
```

Internal Domain Mapping to test Certificate

If you are using Windows follow below steps
* Goto C:\Windows\System32\drivers\etc
* Open hosts file in Administrator Mode
* Add below entry at the end of the File. Replace the External IP which you have got for ingress
<EXTERNAL_IP>   demo.saslab.com
* Open Browser and copy https://demo.saslab.com

If you are using Linux then follow below step. Replace EXTERNAL_IP with IP you get from Ingress

```bash
curl -v -k --resolve demo.azure.com:443:EXTERNAL_IP https://demo.saslab.com
```





