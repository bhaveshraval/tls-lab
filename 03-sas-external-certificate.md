# Lab Environment - Excercise 3
This excercise will provide hands-on on how to create the SAS Viya 4 with customer provided certificates. 

## Lab Objective
In this lab we will look at the steps to secure access into SAS Viya 4 with customer provided TLS certificate. We will examine two different scenarios. 
  1. Certificate to be used in Ingress Controller (NGINX) and inter service communication between pod.
  2. Certificate to be used only in Ingress Controller (NGINX).

## Prerequisite

This are following setup which is required before starting this lab.
* Kubernetes Cluster fully running. You can create this cluster either on cloud or onpremise. You can also make use of terraform scripts from lab 1.
* Kubernetes environment must be accessible from your machine.
* Git should be working from your machine.
* Creating the SAS Viya 4 order and download Deployment Assets (*.tgz) which will contains sas-bases directory.
* TLS Certificate or Certificate chain.
   1. Server and Intermediate Certificate - sas-lab-ingress.crt
   2. Private Key -sas-lab-ingress.key
   3. Root Certificate - ca.crt

## Lab File Strucuture
Below is the Lab File Structure for better understanding.
```
├── sasviya4
    ├── sas-bases
    │   ├── base
    │   └── checksums.txt
    │   └── docs
    │   └── examples
    │   └── overlays
    │   └── README.MD
    └── site-config
    │   ├── mirror.yaml
    │   ├── daily_update_check.yaml
    └── kustomization.yaml 
    └── site.yaml 
```

## Download and Unzip Deployment Assests
Create directory and download deployment assets using order which you created in https://my.sas.com/ portal.
```
mkdir sasviya4
cd sasviya4
# copy the deployment assets which you downloaded to this folder
tar xvfz deploymentAssets.tgz
rm deploymentAssets.tgz
```

The result will be under sasviya4 folder we will get the sas-bases folder which will be use to create the kustomization.yaml file.

## Create the site-config directory
Create directory site-config in sasviya4 directory. This directory will hold files which you can use for user specific components customization. This can be referred to kustomization.yaml.
```
cd sasviya4
mkdir site-config
cp $deploy/sas-bases/examples/security/customer-provided-ingress-certificate.yaml $deploy/site-config/security
```

## Copy Certificate also into site-config directory
The Intermediate Certificate, Root Certificate and Private Key need to be copied into $deploy/site-config/security
```
cp $certificates/. $deploy/site-config/security/.
```

## Certificate Mapping
Modify the tls certificate path in the copied overlay file site-config/security/customer-provided-ingress-certificate.yaml.

```
---
apiVersion: builtin
kind: SecretGenerator
metadata:
  name: sas-ingress-certificate 
files:
  - tls.crt=site-config/security/sas-lab-ingress.crt
  - tls.key=site-config/security/sas-lab-ingress.key
type: "kubernetes.io/tls"
```

Same way modify the root certificate path in the copied overlay file sas-bases/examples/security/customer-provided-ca-certificates.yaml.

```
---
apiVersion: builtin
kind: ConfigMapGenerator
metadata:
  name: sas-customer-provided-ca-certificates
behavior: merge
files:
  - site-config/security/ca.crt
```

## Create the kustomization.yaml
kustomization.yaml file need to be prepared based on components we need during deployment.
This file is either created from scratch as per customer requirement or it can be created by referring to SAS Documentation for initial file creation. Below is sample file.

This is for Option 1
```yaml
      namespace: sasviya4lab
      resources:
      - sas-bases/base
      - sas-bases/overlays/cert-manager-issuer
      - sas-bases/overlays/network/ingress
      - sas-bases/overlays/network/ingress/security
      - sas-bases/overlays/cas-server
      - sas-bases/overlays/internal-postgres
      - sas-bases/overlays/crunchydata
      - sas-bases/overlays/update-checker
      - sas-bases/overlays/cas-server/auto-resources
      configurations:
      - sas-bases/overlays/required/kustomizeconfig.yaml
      transformers:
      - sas-bases/overlays/network/ingress/security/transformers/product-tls-transformers.yaml
      - sas-bases/overlays/network/ingress/security/transformers/ingress-tls-transformers.yaml
      - sas-bases/overlays/network/ingress/security/transformers/backend-tls-transformers.yaml
      - sas-bases/overlays/required/transformers.yaml
      - sas-bases/overlays/internal-postgres/internal-postgres-transformer.yaml
      - sas-bases/overlays/cas-server/auto-resources/remove-resources.yaml
      generators:
      - site-config/postgres/postgres-custom-config.yaml
      - site-config/security/customer-provided-ingress-certificate.yaml
      - site-config/security/customer-provided-ca-certificates.yaml
      configMapGenerator:
      - name: ingress-input
      behavior: merge
      literals:
      - INGRESS_HOST=<<NAME-OF-INGRESS-HOST>>
      - name: sas-shared-config
      behavior: merge
      literals:
      - SAS_SERVICES_URL=https://<<NAME-OF-INGRESS-HOST>>:<<PORT>>
```

This is for Option 2
```yaml
      namespace: sasviya4lab
      resources:
      - sas-bases/base
      - sas-bases/overlays/cert-manager-issuer
      - sas-bases/overlays/network/ingress
      - sas-bases/overlays/network/ingress/security
      - sas-bases/overlays/cas-server
      - sas-bases/overlays/internal-postgres
      - sas-bases/overlays/crunchydata
      - sas-bases/overlays/update-checker
      - sas-bases/overlays/cas-server/auto-resources
      configurations:
      - sas-bases/overlays/required/kustomizeconfig.yaml
      transformers:
      - sas-bases/overlays/network/ingress/security/transformers/truststore-transformers-without-backend-tls.yaml
      - sas-bases/overlays/network/ingress/security/transformers/ingress-tls-transformers.yaml
      - sas-bases/overlays/required/transformers.yaml
      - sas-bases/overlays/internal-postgres/internal-postgres-transformer.yaml
      - sas-bases/overlays/cas-server/auto-resources/remove-resources.yaml
      generators:
      - site-config/postgres/postgres-custom-config.yaml
      - site-config/security/customer-provided-ingress-certificate.yaml
      - site-config/security/customer-provided-ca-certificates.yaml
      configMapGenerator:
      - name: ingress-input
      behavior: merge
      literals:
      - INGRESS_HOST=<<NAME-OF-INGRESS-HOST>>
      - name: sas-shared-config
      behavior: merge
      literals:
      - SAS_SERVICES_URL=https://<<NAME-OF-INGRESS-HOST>>:<<PORT>>
```

## Create the site.yaml file
site.yaml file  is actual Kubernetes Manifest file which is by-product of kustomization.yaml. It is generated by using  below command
```
kustomize build –o site.yaml
```
