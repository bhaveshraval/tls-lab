# Lab - Excercise 2
This excercise will provide hands-on on how to use Cert Manager with NGINX Ingress Controller.

## Lab Objective
In this lab we will look at generating TLS Certificate using Let's Encrypt and using in NGINX Ingress Controller.

## Prequisite
This are following setup which is required before starting this lab.
* Kubernetes Cluster fully running in Azure. You can also make use of terraform scripts from lab 1.
* Kubernetes environment must be accessible from your machine.
* Git should be working from your machine.
* OpenSSL is also required.
* HRLM Chart should also be installed.

For Creating the Azure Kubernetes Service, You can use  below Azure CLI command -
```bash
RG=sas-tls-lab
az aks create \ --resource-group $RG \ --name Cluster01 \ --node-count 3 \ --generate-ssh-keys \ --node-vm-size Standard_B2s \ --enable-managed-identity

az aks get-credentials --name Cluster01 --resource-group $RG
```

## Install Ingress Controller
First step is to create the namespace and deploy NGINX Ingress Controller in kubernetes cluster. NGINX Ingress Controller acts like Front door and in AKS it make use of Load alancer provided by Azure.

```bash
# Create a namespace for your ingress resources
kubectl create namespace ingress-basic

# Add the ingress-nginx repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

# Use Helm to deploy an NGINX ingress controller
helm install nginx-ingress ingress-nginx/ingress-nginx \
    --namespace ingress-basic \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set controller.admissionWebhooks.patch.nodeSelector."beta\.kubernetes\.io/os"=linux

kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller
```

## Add Domain or Get FQDN name
DNS Mapping is important and it need to be mapped to External IPAddress of ingress command.

```bash
az network dns record-set a add-record \
    --resource-group myResourceGroup \
    --zone-name MY_CUSTOM_DOMAIN \
    --record-set-name * \
    --ipv4-address MY_EXTERNAL_IP
```
```bash
# Public IP address of your ingress controller
IP="MY_EXTERNAL_IP"

# Name to associate with public IP address
DNSNAME="demo-saslab-ingress"

# Get the resource-id of the public ip
PUBLICIPID=$(az network public-ip list --query "[?ipAddress!=null]|[?contains(ipAddress, '$IP')].[id]" --output tsv)

# Update public ip address with DNS name
az network public-ip update --ids $PUBLICIPID --dns-name $DNSNAME

# Display the FQDN
az network public-ip show --ids $PUBLICIPID --query "[dnsSettings.fqdn]" --output tsv
```

## Install Cert Manager
Run below help command to install Cert Manager in your kubernetes cluster.

```bash
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --version v1.1.0  \
    --set installCRDs=true \
    --set extraArgs='{--enable-certificate-owner-ref=true}'
```

## Create Let's Encrypt Issuer
Create the Let's Encrypt issuer which will be used for issuing certificate to NGINX Ingress Controller. Copy below Manifest file and create the ClusterIssuer.yaml.
```bash
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: MY_EMAIL_ADDRESS
    privateKeySecretRef:
      name: letsencrypt
    solvers:
    - http01:
        ingress:
          class: nginx
          podTemplate:
            spec:
              nodeSelector:
                "kubernetes.io/os": linux
```

Run the below command to create the resource
kubectl apply - f ClusterIssuer.yaml

Validate the ClusterIssuer
kubectl describe issuer letsencrypt

## Deploy Demo Application into Created Clusters
To test how certificate working, we will deploy one sample application in kubernetes cluster. YAML manifest for sample application is already present in Git. Run below script to deploy.

```bash
kubectl apply -f https://gitlab.com/bhaveshraval/tls-lab/-/blob/main/app-deploy.yaml
```

## Map TLS certificate in Ingress Controller 
Create the ingress-controller.yaml and copy and paste above script. 

```bash
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: saslab-ingress
  namespace: ingress-basic
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$1
    nginx.ingress.kubernetes.io/use-regex: "true"
    cert-manager.io/cluster-issuer: letsencrypt
spec:
  tls:
  - hosts:
    - <FQDN>
    secretName: tls-secret
  rules:
  - host: <FQDN>
    http:
      paths:
      - path: /(.*)
        pathType: Prefix
        backend:
          service:
            name: saslab
            port:
              number: 80
```

Deploy the Ingress Controller
```bash
kubectl apply -f ingress-controller.yaml
```

Cert-manager will read these annotations in Ingress and use them to create a certificate, which you can request and see:

```bash
kubectl get certificate --namespace ingress-basic
```

## Check if the TLS is ready to be tested

```bash
kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller
```

Internal Domain Mapping to test Certificate

If you are using Windows follow below steps
* Open Browser and copy https://<FQDN>

If you are using Linux then follow below step.

```bash
curl https://demo.saslab.com
```
